<?php

class TelegrafText
{
    public $text, $title, $author, $slug, $published;

    public function __construct()
    {
        echo 'Введите имя автора: ' . PHP_EOL;;
        $this->author = readline( '');
        $this->published = date('d_M_Y_H_i_s');
        $this->slug = $this->author . $this->published;
    }

    public function storeText()
    {

        $TextArray = ['title' => [$this->title],
                      'text' => [$this->text],
                      'author' => [$this->author],
                      'published' => [$this->published]];

        $SeriesTextArray = serialize($TextArray);
        file_put_contents($this->slug, $SeriesTextArray);
        echo $SeriesTextArray;
    }

    public function loadText()
    {
        $unTextArray = [];
        if (file_exists($this->slug) === false) {
            echo 'Файл не найден'. PHP_EOL;
            return;
        }
        if (file_exists($this->slug) == true) {

            $unTextArray = unserialize(file_get_contents($this->slug));
        }
        echo 'Вы ввели текст:' . PHP_EOL;;
        print_r ($unTextArray['text']);
    }

    public function editText()
    {
        echo 'Введите заголовок сообщения: '. PHP_EOL;
        $newTitle = readline( ' ');
        $this->title = $newTitle;
        echo 'Введите текст: '. PHP_EOL;
        $newText = readline( ' ');
        $this->text = $newText;
    }

}

$proverka = new TelegrafText();

$proverka->editText();

$proverka->storeText();

$proverka->loadText();



